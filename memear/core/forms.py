from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .words import ADVERBIOS, SUSTANTIVOS, ADJETIVOS
import random
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV3


def generate_username():
    sus = random.choice(SUSTANTIVOS).title()
    adv = random.choice(ADVERBIOS).title()
    adj = random.choice(ADJETIVOS).title()
    return f"{sus}{adv}{adj}{random.randint(1,1000)}"


class LoginForm(AuthenticationForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'placeholder': 'Su dirección de e-mail',
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Ingrese contraseña',
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))


class SignUpForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'recaptcha')

    def __init__(self, *args, **kwargs):
            super(SignUpForm, self).__init__(*args, **kwargs)

    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'placeholder': 'Su dirección de e-mail',
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Ingrese contraseña',
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Reingrese contraseña',
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))
    recaptcha = ReCaptchaField(widget=ReCaptchaV3)