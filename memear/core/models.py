from django.db import models
from django.contrib.auth.models import User
import pydenticon
import hashlib


foreground = [ "rgb(45,79,255)", #AZUL
               "rgb(254,180,44)", #AMARILLO
               "rgb(226,121,234)", #ROSA
               "rgb(30,179,253)", #CELESTE
               "rgb(232,77,65)", #ROJO
               "rgb(255,153,51)", #NARANJA
               "rgb(49,203,115)", #VERDE
               "rgb(141,69,170)" ] #VIOLETA

background = "rgba(255,255,255,0)"

generator = pydenticon.Generator(9, 9, digest=hashlib.sha1,
                                 foreground=foreground, background=background)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    pfp = models.ImageField(default='default.jpg', upload_to='pfps')

    def __str__(self):
        return f'{self.user.username} Profile' 
    
    
