from django.shortcuts import render, redirect
from .models import Profile, generator
from .forms import SignUpForm, generate_username, LoginForm
import io
from django.core.files.images import ImageFile
from django.http import JsonResponse, HttpResponseRedirect
from django.templatetags.static import static
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages


def generate_username_json(request):
    return JsonResponse({'username': generate_username()})


def index(request):
    return render(request, 'core/index.html', {
        'fresh' : int(request.GET.get("fresh", 1)) == 1,
        'top' : int(request.GET.get("top", 0)) == 1,
        'upvoted' : int(request.GET.get("upvoted", 0)) == 1,
    })


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_profile = Profile.objects.create(user=new_user)
            identicon_png = generator.generate(new_user.username, 180, 180, output_format="png")
            image = ImageFile(io.BytesIO(identicon_png), name='new_user.id')
            new_profile.pfp.save(f"{new_user.id}.png", image, save=True)
            new_profile.save()
            messages.success(request, 'El usuario se creó correctamente. Inicie sesión.')
            return redirect('/login_user/')
        else:
            print(form.errors)
            messages.error(request, 'Ocurrió un error al crear el usuario.')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = SignUpForm()
    return render(request, 'core/signup.html', {
        'form': form,
        'username': generate_username()
    })


def login_user(request):
    if request.method == 'POST':
        email = request.POST["email"]
        password = request.POST["password"]
        user_object = User.objects.get(email__exact=email)
        username = user_object.username
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user_object)
            return redirect('/')
        else:
            messages.success(request, 'Email o contraseña incorrectos.')
            return redirect('/login_user/')
    else:
        form = LoginForm()
    return render(request, 'core/login.html', {
        'form': form,
    })


def logout_user(request):
    logout(request)
    return redirect('/')

