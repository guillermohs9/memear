from django.contrib import admin
from .models import Meme, Comment, Vote

admin.site.register(Meme)
admin.site.register(Vote)
admin.site.register(Comment)
