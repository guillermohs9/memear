from django import forms

from .models import Meme

class NewMemeForm(forms.ModelForm):
    class Meta:
        model = Meme
        fields = ('title', 'image', 'ig_url')

    title = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Un título ingenioso',
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))

    image = forms.FileField(required=False, widget=forms.FileInput(attrs={
        'class': 'block w-full focus:outline-none absolute inset-0 opacity-0 cursor-pointer',
    }))

    ig_url = forms.URLField(required=False, widget=forms.URLInput(attrs={
        'class': 'w-full py-3 px-6 rounded-xl border dark:border-gray-300 border-gray-600 bg-gray-200 dark:bg-gray-700 text-gray-500 dark:text-gray-400',
    }))