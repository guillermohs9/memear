from django.db import models
from django.contrib.auth.models import User
import secrets
import os
import zoneinfo
from datetime import timezone
from django.core.exceptions import ValidationError


ARG = zoneinfo.ZoneInfo('America/Buenos_Aires')


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.png', '.webp', '.gif', '.bmp', '.jpeg', '.mp4']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Error: Tipo de archivo no soportado.')


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    fext = os.path.splitext(filename)[1]
    return "{0}/{1}".format(instance.created_by.id, f"{secrets.token_hex(10)}{fext}")


class Meme(models.Model):
    title = models.CharField(max_length=100, blank=False, null=False, default='')
    image = models.FileField(upload_to=user_directory_path, blank=False, null=False, validators=[validate_file_extension])
    created_by = models.ForeignKey(User, related_name='memes', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return f"{self.created_by.username}-{self.image.url}"
    
    def fecha(self):
        return self.created_at.replace(tzinfo=timezone.utc).astimezone(ARG).strftime('%d/%m/%y %H:%M')
    
    @property
    def upvotes_count(self):
        return self.votes.filter(vote=True).all().count()
    
    @property
    def downvotes_count(self):
        return self.votes.filter(vote=False).all().count()

    @property
    def net_vote_count(self):
        net_count = self.upvotes_count - self.downvotes_count
        if net_count > 1000:
            return round(net_count, -2) / 1000
        return net_count
        
    def voted(self, user):
        return self.votes.filter(user=user).filter(vote__isnull=False).first() is not None
    
    def upvoted(self, user):
        return self.votes.filter(user=user).filter(vote=True).first() is not None
    
    def downvoted(self, user):
        return self.votes.filter(user=user).filter(vote=False).first() is not None


class Comment(models.Model):
    meme = models.ForeignKey(Meme, related_name='comments', on_delete=models.CASCADE)
    content = models.CharField(max_length=255)


class Vote(models.Model):
    class Meta:
        unique_together = [('meme', 'user')]

    meme = models.ForeignKey(Meme, related_name='votes', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.BooleanField(null=True)


from urllib.parse import urlparse
import instaloader

L = instaloader.Instaloader(dirname_pattern=None, save_metadata=False, compress_json=False, download_video_thumbnails=False, post_metadata_txt_pattern='')

def get_shortcode_from_url(url):
    parsed = urlparse(url)
    type, shortcode = parsed.path.split('/')[1:3]
    return type, shortcode


def download_post(shortcode, target):
    L.title_pattern=f'{{typename}}'
    post = instaloader.Post.from_shortcode(L.context, shortcode)
    if L.download_post(post, target=target):
        print(post.title)
        
def delete_file(filename):
    try:
        os.remove(filename)
    except OSError:
        pass