from django.urls import path
from . import views

app_name = 'meme'


urlpatterns = [
    path('new/', views.new_meme, name='new_meme'),
    path('upvote/', views.upvote_meme, name='upvote_meme'),
    path('downvote/', views.downvote_meme, name='downvote_meme'),
    path('memes.json', views.listing_api, name="memes-api"),
]