from django.shortcuts import redirect, render
from django.http import JsonResponse, HttpResponseNotFound, HttpResponseRedirect
from .models import Meme, Vote
from django.contrib.auth.decorators import login_required
from .forms import NewMemeForm
from django.core.paginator import Paginator
from django.db.models import  Q, Count
from meme.models import download_post, get_shortcode_from_url
import os
from django.core.files.images import ImageFile
import io

@login_required
def upvote_meme(request):
    meme_id = request.GET['id']
    meme = Meme.objects.filter(id=meme_id).first()
    if not meme:
        return HttpResponseNotFound("Error")
    vote = Vote.objects.filter(meme=meme).first()
    if vote:
        if vote.vote is True:
            vote.vote = None
        else:
            vote.vote = True
    else:
        vote = Vote(vote=True, meme=meme, user=request.user)
    vote.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return JsonResponse({'result': 200})


@login_required
def downvote_meme(request):
    meme_id = request.GET['id']
    meme = Meme.objects.filter(id=meme_id).first()
    if not meme:
        return HttpResponseNotFound("Error")
    vote = Vote.objects.filter(meme=meme).first()
    if vote:
        if vote.vote is False:
            vote.vote = None
        else:
            vote.vote = False
    else:
        vote = Vote(vote=False, meme=meme, user=request.user)
    vote.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return JsonResponse({'result': 200})


@login_required
def new_meme(request):
    if request.method == "POST":
        form = NewMemeForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['ig_url']:
                meme = Meme()
                _, code = get_shortcode_from_url(form.cleaned_data['ig_url'])
                download_post(code, f'dl{request.user.id}')
                with open(os.path.join(f'dl{request.user.id}', os.listdir(f'dl{request.user.id}')[0]), "rb") as fh:
                    buf = io.BytesIO(fh.read())
                image = ImageFile(buf, name=os.listdir(f'dl{request.user.id}')[0])
                meme.image.save(os.listdir(f'dl{request.user.id}')[0], image, save=True)
            else:
                meme = form.save(commit=False)
            meme.created_by = request.user
            meme.save()
            vote = Vote(vote=True, meme=meme, user=request.user)
            vote.save()
            return redirect('/')
    else:
        form = NewMemeForm()
    return render(request, 'meme/new_meme.html', {
        'form': form,
        'title': 'Nuevo Meme'
    })





def listing_api(request):
    page_number = request.GET.get("page", 1)
    per_page = request.GET.get("per_page", 2)
    fresh = int(request.GET.get("fresh")) == 1
    top = int(request.GET.get("top")) == 1
    upvoted = int(request.GET.get("upvoted")) == 1
    if fresh:
        memes = Meme.objects.order_by('-created_at')
    if top:
        memes = Meme.objects.filter(votes__vote=True).annotate(net_votes=Count('votes', filter=Q(votes__vote=True))).order_by('-net_votes').order_by('-created_at')
    if upvoted:
        memes = Meme.objects.filter(votes__user=request.user).filter(votes__vote=True)
    paginator = Paginator(memes, per_page)
    page_obj = paginator.get_page(page_number)
    data = [{"title": meme.title,
            "id": meme.id,
            "upvoted": meme.upvoted(request.user),
            "upvotes_count": meme.upvotes_count,
            "downvotes_count": meme.downvotes_count,
            "net_vote_count": meme.net_vote_count,
            "downvoted": meme.downvoted(request.user),
            "image_url": meme.image.url,
            "pfp_url": meme.created_by.profile.pfp.url,
            "created_by": meme.created_by.username,
            "created_at": meme.fecha()} for meme in page_obj.object_list]
    payload = {
        "page": {
            "current": page_obj.number,
            "has_next": page_obj.has_next(),
            "has_previous": page_obj.has_previous(),
        },
        "data": data,
    }
    return JsonResponse(payload)